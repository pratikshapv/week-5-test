<center><img src = "https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-RP0103EN-SkillsNetwork/labs/module%201/images/IDSNlogo.png" width = 250></center>

<h1 align=center><font size = 5>Hands-on Lab: Creating and Querying Database using phpMyAdmin graphical user interface (GUI) tool</h1>

### Welcome!

In this hands-on lab, we will create and query database objects using phpMyAdmin graphical user interface (GUI) tool

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

**Ontario public schools enrollment dataset** database has been used in this lab.

Here you will be creating tables and querying data by downloading the below mentioned csv data files.

<a href="BOARD.csv">BOARD_CSV</a>

<a href ="SCHOOL.csv">SCHOOL_CSV</a>

###  Create a database

1. Go to **Terminal > New Terminal** to open a terminal from the side by side launched Cloud IDE.

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.1.png)

</br>

2. Start MySQL service session in the Cloud IDE using the command below in the terminal. Find your MySQL service session password from the highlighted location of the terminal shown in the image below. Note down your MySQL service session password because you may need to use it later in the lab.

    ```
    start_mysql
    ```

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.2.png)

</br>

3. Copy your phpMyAdmin weblink from the highlighted location of the terminal shown in the image below. Past it into the address bar in a new tab of your web browser. This will open the phpMyAdmin tool.

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.3.png)

</br>

4. You will see the phpMyAdmin GUI tool.

![image](./images/Launch_in_IBM_Cloud_Pak.PNG)

</br>

5. In the tree-view, click **New** to create a new empty database. Then enter **Ontario_public_schools enrollment_dataset** as the name of the database and click **Create**.

    The encoding will be left as **utf8mb4\_0900\_ai_ci**. UTF-8 is the most commonly used character encoding for content or data.

    The screen will look like this :

![image](./images/db.png)

<a id="ref4o"></a>

#### A. Create the tables in database

* Go to sql section and Lets create the BOARD table in the database.
<details>
<summary>Click here to view/hide hint</summary>
<p>

```
# Fill in the ...
CREATE ... BOARD (
                            B_ID ...(6) NOT ..., 
                            B_NAME ...(75) NOT ..., 
                            TYPE ...(50) NOT ..., 
                            LANGUAGE ...(50), 
                            ... KEY (B_ID))
```

</details>

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
CREATE TABLE BOARD (
                            B_ID CHAR(6) NOT NULL, 
                            B_NAME VARCHAR(75) NOT NULL, 
                            TYPE VARCHAR(50) NOT NULL, 
                            LANGUAGE VARCHAR(50), 
                            PRIMARY KEY (B_ID))
```

</details>


* Check if tables are created successfully.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
SELECT * FROM BOARD
```

</details>

* Now click on the **Ontario_public_schools enrollment_dataset** showing on the left side of the screen to go back to the database folder, lets create the SCHOOL table in the database. 

<details>
<summary>Click here to view/hide hint</summary>
<p>

```
# Fill in the ...
CREATE ... SCHOOL (
                  B_ID ...(6) NOT NULL, 
                  S_ID ...(6) NOT NULL, 
                  S_NAME ...(100), 
                  LEVEL ...(70), 
                  ENROLLMENT .... (10),
                  PRIMARY ... (B_ID, S_ID))
```
</details>

<details>
<summary>Click here to view/hide solution</summary>
<p>

```CREATE TABLE SCHOOL (
                  B_ID CHAR(6) NOT NULL, 
                  S_ID CHAR(6) NOT NULL, 
                  S_NAME VARCHAR(100), 
                  LEVEL VARCHAR(70), 
                  ENROLLMENT INTEGER (10),
                  PRIMARY KEY (B_ID, S_ID))
```
</details>

* Check if tables are created successfully.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
SELECT * FROM SCHOOL
```
</details>

<a id="ref4o"></a>

#### B. Load the data into the database tables

* Click on the BOARD heading on the left corner of screen to go inside BOARD table. Print column details for the tables BOARD.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
DESCRIBE BOARD
```
</details>

* Click on the SCHOOL heading on the left corner of screen to go inside SCHOOL table. Print column details for the tables SCHOOL.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
DESCRIBE SCHOOL
```
</details>

* Load the data from the BOARD_CSV into the BOARD dataframe. To load the data BOARD click on the heading on the left corner of screen to go inside BOARD table. Then click on **Import** tab and choose the downloaded BOARD_CSV file. Click on **Go** button.

![image](./images/load_data_board.png)

* Display initial data from the BOARD dataframe.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
SELECT * FROM BOARD
```
</details>

* Load the data from the SCHOOL_CSV into the BOARD dataframe. To load the data BOARD click on the heading on the left corner of screen to go inside BOARD table. Then click on **Import** tab and choose the downloaded SCHOOL_CSV file. Click on **Go** button.

![image](./images/load_data_school.png)

* Display initial data from the SCHOOL dataframe.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
SELECT * FROM SCHOOL
```
</details>

<a id="ref4o"></a>

#### C. Fetch the data from database

Get the elementary school data from the database from both tables in descending sequence.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
SELECT S.ENROLLMENT as ENROLLMENT 
FROM SCHOOL S, BOARD B 
WHERE B.B_NAME = 'Toronto DSB' and B.B_ID = S.B_ID 
and S.LEVEL = 'Elementary' 
ORDER BY ENROLLMENT DESC
```

</details>

Get the secondary school enrollments query in descending sequence.

<details>
<summary>Click here to view/hide solution</summary>
<p>

```
SELECT S.ENROLLMENT as ENROLLMENT 
FROM SCHOOL S, BOARD B 
WHERE B.B_NAME = 'Toronto DSB' and B.B_ID = S.B_ID 
and S.LEVEL = 'Secondary' 
ORDER BY ENROLLMENT DESC
```

</details>

<a id="ref4o"></a>

### Summary
In this lab you created and queried database objects using phpMyAdmin graphical user interface (GUI) tool.


# 

<h3> Congratulations! You have completed this Lab. <h3/>


# Author(s)

[Pratiksha Verma](https://www.linkedin.com/in/pratiksha-verma-6487561b1/)

## Changelog

| Date       | Version | Changed by      | Change Description      |
| ---------- | ------- | --------------- | ----------------------- |
| 2022-02-10 | 0.1     | Pratiksha verma | Created Initial Version |

## <h3 align="center"> IBM Corporation 2022. All rights reserved. <h3/>




















                           
